# First steps with your sausage
This will configure your project on Sausage Cloud with a sensible set of the following:

  * A default network
  * A default subnet
  * A default router
  * Some sensible security group rules

With those pieces in place, it'll boot a VM for you then associate a floating IP, which you can then use to remotely access your machine.

## Installation
Firstly you need to install [Terraform](https://terraform.io) (v1.0+).  Once that's done, clone this repo and run the `terraform init` command:
```code
git clone https://gitlab.com/sausages/onboarding.git
[..]
terraform init
```

Then you need to create a file to store your OpenStack API authentication details which will be used by Terraform (and the OpenStack CLI):

```
mkdir -p ~/.config/openstack
vim ~/.config/openstack/clouds.yaml
```

The contents of `clouds.yaml` should be:

```yaml
clouds:
  sausage:
    identity_api_version: 3
    interface: public
    region_name: Bunker
    auth:
      domain_name: 'Default'
      project_domain_name: 'Default'
      project_name: 'your_project'
      username: 'your_username'
      password: 'your_password'
      auth_url: 'https://compute.sausage.cloud:5000/v3'
```

Replace `your_project`, `your_username` and `your_password` with the corresponding values.

Finally, set an environment variable in your shell which tells any application that makes use of this file to select the right 'cloud' entry:

```shell
export OS_CLOUD="sausage"
```

## Configuration
There's only two variables you need to set, both of which are in the terraform.tfvars file.  These are related to the SSH public key you'll use to connect to your VM.  With those set, you can preview the infrastructure changes that Terraform will make on your behalf:

```shell
terraform plan
```

## Usage
To deploy your virtual infrastructure, run the following command:

```shell
terraform apply

[..]

Apply complete! Resources: 9 added, 0 changed, 0 destroyed.

Outputs:

ip = 93.186.40.101
```

At the end of the runm, assuming it's successful, it'll echo an output which is the external ('floating') IP of your new VM.  To connect:

```shell
ssh ubuntu@93.186.40.101
```

