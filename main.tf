resource "openstack_compute_keypair_v2" "keypair" {
  name       = var.ssh_keypair_name
  public_key = var.ssh_public_key
}

resource "openstack_networking_network_v2" "default" {
  name = "default"
}

resource "openstack_networking_subnet_v2" "default" {
  name        = "192.168.1.0/24"
  network_id  = openstack_networking_network_v2.default.id
  cidr        = "192.168.1.0/24"
  ip_version  = 4
  enable_dhcp = true
}

resource "openstack_networking_router_v2" "default" {
  name                = "default"
  admin_state_up      = true
  external_network_id = "5617d17e-fdc1-4aa1-a14b-b9b5136c65af"
}

resource "openstack_networking_router_interface_v2" "default_int" {
  router_id = openstack_networking_router_v2.default.id
  subnet_id = openstack_networking_subnet_v2.default.id
}

resource "openstack_compute_secgroup_v2" "mysg" {
  name        = "mysg"
  description = "Sensible security group"

  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 80
    to_port     = 80
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = -1
    to_port     = -1
    ip_protocol = "icmp"
    cidr        = "0.0.0.0/0"
  }
}

resource "openstack_compute_instance_v2" "myvm" {
  name            = "myvm"
  image_name      = "Ubuntu 22.04 (20230317)"
  flavor_name     = "frankfurter"
  key_pair        = var.ssh_keypair_name
  security_groups = ["mysg"]

  network {
    uuid           = openstack_networking_network_v2.default.id
    access_network = true
  }
}

resource "openstack_networking_floatingip_v2" "myvm_external_ip" {
  pool       = "internet"
  depends_on = [openstack_networking_router_interface_v2.default_int]
}

resource "openstack_compute_floatingip_associate_v2" "myvm_external_ip" {
  floating_ip = openstack_networking_floatingip_v2.myvm_external_ip.address
  instance_id = openstack_compute_instance_v2.myvm.id
}

output "ip" {
  value = openstack_networking_floatingip_v2.myvm_external_ip.address
}
